#pragma once

typedef struct limit_value_range    limit_value_range_t;
typedef struct limit_selection      limit_selection_t;
typedef struct limit_solver         limit_solver_t;

typedef size_t                      limit_index_t;
typedef float                       limit_progress_t;
typedef float                       limit_value_t;
typedef void*                       limit_user_data_t;


struct limit_value_range
{
    const limit_value_t* begin;
    const limit_value_t* end;
};

struct limit_selection
{
    limit_index_t     index;
    limit_progress_t  progress;
};

struct limit_solver
{
    limit_user_data_t   instance;
    bool                (*interface)(limit_user_data_t&, const limit_selection_t&, limit_value_range_t&, limit_value_t&);
};


bool limit_verify(
    const limit_value_range_t&  range
);

bool limit_select(
    const limit_value_range_t&  range, 
    const limit_value_t&        value,
    limit_selection_t&          selection
);

limit_index_t limit_solve(
    limit_solver_t      self,
    limit_value_range_t range, 
    limit_value_t       value
);