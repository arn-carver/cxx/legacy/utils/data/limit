#include "limit.h"

limit_value_t limit_value_to_progress(
    const limit_value_t& value, 
    const limit_value_t& first,
    const limit_value_t& last
) {
    return (value - first) / (last - first);
}

bool limit_value_match(
    const limit_value_t& value, 
    const limit_value_t& first,
    const limit_value_t& last
) {
    return value >= first && last >= value;
}



bool limit_verify(
    const limit_value_range_t&  range
) {
    if ((range.end - range.begin) < 2)
    {
        return false;
    }
    
    const limit_value_t* it = range.begin;
    const limit_value_t* end = range.end - 1;
    
    while (it != end)
    {
        if (it[0] >= it[1])
        {
            return false;
        }
        
        ++it;
    }
    
    return true;
}

bool limit_select(
    const limit_value_range_t&  range, 
    const limit_value_t&        value,
    limit_selection_t&          selection
) {
    limit_index_t count = range.end - range.begin; 
    
    if (count > 1)
    {
        const limit_value_t& first = range.begin[0];
        const limit_value_t& last = range.begin[count - 1];
        
        if (first >= value)
        {
            selection.index = 0;
            selection.progress = limit_value_t(0);
        }
        else if (last <= value)
        {
            selection.index = count - 2;
            selection.progress = limit_value_t(1);
        }
        else
        {
            selection.progress = limit_value_to_progress(value, first, last);
            
            if (count > 2)
            {
                limit_value_t ratio = limit_value_t(1) / limit_value_t(count);
                
                selection.index = limit_index_t(ratio * selection.progress);
                
                const limit_value_t& entry = range.begin[selection.index]; 
                const limit_value_t* it = &entry;
                const limit_value_t* end;
                
                int direction(0);
                
                if (entry > value)
                {
                    direction = -1;
                    
                    end = range.begin - 1;
                }
                else if (entry < value)
                {
                    direction = +1;
                    
                    end = range.end - 1;
                }
                else
                {
                    selection.progress = limit_value_t(0);
                    
                    return true;
                }
                
                while (it != end)
                {
                    const limit_value_t& current = it[0];
                    const limit_value_t& second = it[1];
                    
                    if (limit_value_match(value, current, second))
                    {
                        selection.index = it - range.begin;
                        selection.progress = limit_value_to_progress(value, current, second);
                        
                        return true;
                    }
                    else
                    {
                        it += direction;
                    }
                }
                
                return false;
            }
            else
            {
                selection.index = 0;
            }
        }
        
        return true;
    }
    else if (count > 0)
    {
        selection.index = 0;
        selection.progress = limit_value_t(0);
        
        return true;
    }
    else
    {
        return false;
    }
}

limit_index_t limit_solve(
    limit_solver_t      self, 
    limit_value_range_t range, 
    limit_value_t       value
) {
    limit_index_t     selections(0);
    limit_selection   selection;
    
    do
    {
        if (limit_select(range,value,selection))
        {
            ++selections;
        }
        else
        {
            break;
        }
        
    } while (self.interface(self.instance,selection,range,value));
    
    return selections;
}
